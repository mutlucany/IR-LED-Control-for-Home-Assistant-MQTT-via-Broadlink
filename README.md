# IR LED Control for Home Assistant MQTT via Broadlink
Node-RED flow for OSRAM or similar IR LEDs to control with Home Assistant and MQTT via Broadlink RM devices

![ss.png](ss.png)

This flow will convert values from Home Assistant or any other MQTT client to IR codes and sends them via the Broadlink node. So functions are limited to whatever the remote has. 

- OSRAM bulbs have **4 steps of brightness**. Since the remote can't set a brightness but only increase or reduce it, the brightness setting will be remembered and converted accordingly.
- The colour choice is also limited compared to what Home Assistant can send. So, RGB colour codes will be automatically converted to **nearest available colour** whatever the remote has. 
- Near to **white** colours will send a "W" command on the remote.
- All **effects** the remote has will be available. 

## Requirements
- Any Broadlink RM universal remote
- An MQTT Broker
- Node-RED
	- [node-red-contrib-msg-resend](https://github.com/bartbutenaers/node-red-contrib-msg-resend)

## Instructions
### Home Assistant Configuration
- Add [Broadlink integration](https://www.home-assistant.io/integrations/broadlink/) if you don't already have.
- Copy the content of the `configuration.yaml` file in the repo to `configuration.yaml` of Home Assistant. 
- Make edits if needed. (maybe change the name)
- Restart Home Assistant.

### Node-RED Configuration
- Import the `flows.json` file.
- If not configured, open a **MQTT** node (such as "light/osram/set) and enter MQTT username and password.
- Deploy.