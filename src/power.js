let getMsg = msg.payload;
let mantar = global.get('mantar');

if (!mantar) {
    mantar = {
        power: "ON",
        brightness: 255,
        color: [255, 255, 255],
        effect: "None"
        
    }
} else {
    mantar.power = getMsg;
}
global.set('mantar', mantar);

msg = {};
msg.payload = {};
msg.payload.action = 'send';

if (mantar.power === "ON") {
    msg.payload.data = "JgBYAAABKZURFBEUEhMSFBEUERQSExMSEjgROBI4ETgSOBE4EjgROBI4EDkSOBEUEhMRFBEVERQRFBITEhMSOBE4EjgROBI4EQAFHwABKEsRAAxWAAEoSxIADQU="
} else {
    msg.payload.data = "JgBQAAABKJYRFBEUEhMSExIUEBURFBITEjgROBI4ETgSOBE4EjgROBIUEDkSNxIUERQRFBITERUROBITEhQQORI4EDkSNxI4EQAFHwABKEsSAA0FAAAAAAAAAAA=" 
}
return msg;
