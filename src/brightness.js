let getMsg = msg.payload;
let mantar = global.get('mantar');

if (!mantar) {
    mantar = {
        power: "ON",
        brightness: 255,
        last: 5,
        color: [255, 255, 255],
        effect: "Smooth"
        
    }
} else {
    mantar.brightness = parseFloat(getMsg);
}


msg = {};
msg.payload = {};
msg.payload.action = 'send';

if (mantar.brightness > 200) {
    msg.payload = 5 - mantar.last;
    mantar.last = 5;
} else if (mantar.brightness > 150) {
    msg.payload = 4 - mantar.last;
    mantar.last = 4;
} else if (mantar.brightness > 100) {
    msg.payload = 3 - mantar.last;
    mantar.last = 3;
} else if (mantar.brightness > 50) {
    msg.payload = 2 - mantar.last;
    mantar.last = 2;
} else {
    msg.payload = 1 - mantar.last;
    mantar.last = 1;
}

global.set('mantar', mantar);
return msg;
