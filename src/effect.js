let getMsg = msg.payload;
let mantar = global.get('mantar');

if (!mantar) {
    mantar = {
        power: "ON",
        brightness: 255,
        color: [255, 255, 255],
        effect: "None"
        
    }
} else {
    mantar.effect = getMsg;
}
global.set('mantar', mantar);

msg = {};
msg.payload = {};
msg.payload.action = 'send';

if (mantar.effect === "Smooth") {
    msg.payload.data = "JgBgAAABKJUSExIUERQRFBITEhQQFREUEjgROBI4ETgSOBA5EjgROBI4ETgSOBEUETgSFBEUEhMSExIUERQQORITEjgSOBE4EQAFHwABKEsSAAxSAAEnTBEADFIAAShLEgANBQAAAAAAAAAA"
} else if (mantar.effect === "Strobe") {
    msg.payload.data = "JgBgAAABKJUSFBAVERQRFBIUERQRFBITETkROBI4ETgROBI4ETgSOBI3EjgRFBITETkRFBITEhMRFREUEjcSOBEUEjgROBI4EQAFHwABKEsRAAxSAAEoShIADFIAASdLEgANBQAAAAAAAAAA"
} else if (mantar.effect === "Flash") {
    msg.payload.data = "JgBYAAABJ5YRFBEUEhMSExIUERQRFBEUEjgROBI4ETgRORE4EjgROBI4ETgSOBE4ERQSExIUERQRFBITEhMRFRE4EjgROBI3EgAFHwABJ0sSAAxSAAEoShIADQU="
} else {
    msg.payload.data = "JgBwAAABKZURFBITEhMSFBEUERQRFBIUETgRORE4EjgQORI4ETgSOBE4EjgRFBEUEhMSExIUERQRFBITEjgROBI4EjcSOBI3EQAFIAABKEsRAAxSAAEqSREADFIAASlKEgAMUgABKEsRAAxSAAEnTBEADQUAAAAAAAAAAA==" 
}
return msg;
