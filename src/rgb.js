let getMsg = msg.payload;
let mantar = global.get('mantar');

mantar.color = getMsg.split(',');

global.set('mantar', mantar.color);

msg = {};
msg.payload = {};
msg.payload.action = 'send';

const colors = [
    ['red', 255, 0, 0],
    ['green', 0, 255, 0],
    ['blue', 0, 0, 255],
    ['white', 255, 255, 255],
    ['white', 255, 212, 169],
    ['1', 255, 64, 0],
    ['2', 0, 255, 128],
    ['3', 128, 0, 255],
    ['4', 255, 128, 0],
    ['5', 0, 255, 64],
    ['6', 255, 0, 255],
    ['7', 255, 191, 0],
    ['8', 0, 255, 255],
    ['9', 255, 0, 255],
    ['10', 255, 255, 0],
    ['11', 0, 128, 255],
    ['12', 255, 0, 128],
]

function get_closest_color(colors, [r2, g2, b2]) {
    const [[closest_color_id]] = (
        colors
            .map(([id, r1, g1, b1]) => (
                [id, Math.sqrt((r2 - r1) ** 2 + (g2 - g1) ** 2 + (b2 - b1) ** 2)]
            ))
            .sort(([, d1], [, d2]) => d1 - d2)
    );
    return closest_color_id;
}

const closest_color = get_closest_color(colors, mantar.color);
switch (closest_color) {
    case 'red':
        msg.payload.data = 'JgBYAAABKZURFBITEhQRFBEUERQRFREUEDkRORE4EjgROBI4ETgSOBEUEhMSFBE4EhMSFBEUERQSOBE4EjgRFBE4ETkROBI4EQAFIgABKEsRAAxUAAEpShIADQU='
        break;
    case 'green':
        msg.payload.data = 'JgBgAAABKZURFRAVERQRFBIUEBUSExIUETgSOBE4EjgRORE4EjgSNxI4EhMSFBE4EhQRFBITEhQRFBI3EjgSExI4EjgROBI4EQAFIgABKUsRAAxXAAEpSxEADFQAAShLEgANBQAAAAAAAAAA'
        break;
    case 'blue':
        msg.payload.data = 'JgBgAAABKJUSExITEhQRFBITEhMSFBEUEjcSOBI3EjgSOBE4EjgROBITETkRFBI4ERQRFBITEhQROBITEjgRFBI4ETgSOBE4EgAFHwABKUoSAAxTAAEoSxIADFIAAShLEgANBQAAAAAAAAAA'
        break;
    case 'white':
        msg.payload.data = 'JgBwAAABKZURFBITEhMSFBEUERQRFBIUETgRORE4EjgQORI4ETgSOBE4EjgRFBEUEhMSExIUERQRFBITEjgROBI4EjcSOBI3EQAFIAABKEsRAAxSAAEqSREADFIAASlKEgAMUgABKEsRAAxSAAEnTBEADQUAAAAAAAAAAA=='
        break;
    case '1':
        msg.payload.data = 'JgBQAAABKJUSFBEUEBURFBITEhQRFBEUEjgROBE4EjgROBI4EjcSOBEUEhMSOBE4EhMSFBEUERQROBI4EhMRFBI4ETgSOBE4EgAFHgABKEsSAA0FAAAAAAAAAAA='
        break;
    case '2':
        msg.payload.data = 'JgBYAAABKJURFBEUEhMSExIUERQRFBITEjgROBI4ETgSOBE4EjgROBI4ERQROBI4ERQSExITEhQRFBE4EhQRFBE4EjgROBI4EQAFHwABKEsRAAxSAAEnSxIADQU='
        break;
    case '3':
        msg.payload.data = 'JgBYAAABKJURFBEVERQSExITEhQQFREUETkROBI3EjgSNxI4ETgSOBEUEjgROBI4EBURFBITEhQROBITEhMSFBE4EjgROBI4EQAFHwABKEsRAAxSAAEoSxEADQU='
        break;
    case '4':
        msg.payload.data = 'JgBYAAABJ5YSFBAVERQSExIUEBURFBITEjgROBE5EDkSOBE4ETkROBITEhQQFREUETgSFBEUEBUSOBE4ETgSOBEUEjgROBI4EQAFHwABJ0sSAAxTAAEoShIADQU='
        break;  
    case '5':
        msg.payload.data = 'JgBYAAABKJUSFBEUERQSExITERUQFREUETgSOBE4EjgROBE5ETgSOBE4ERQSFBEUETgSExIUERQRFBE4EjgROBIUETgROBI4EAAFIAABJ0wRAAxRAAEoSxEADQU='
        break;
    case '6':
        msg.payload.data = 'JgBQAAABKJUSFBAVERQRFBITEhQQFREUEjcSOBE4EjgROBE5ETgSOBEUETgSExIUEDkSExITEhQROBITEjgROBQREjgROBI4EQAFHwABKEsRAA0FAAAAAAAAAAA='
        break;
    case '7':
        msg.payload.data = 'JgBQAAABKJURFBEUEhMSFBEUERQRFBITEjgSNxI4ETgSOBE4ETkROBITEhMROREUEjgRFBAVEhMRORE4EhMSOBEUEjgQORI4EQAFHwABKEsRAA0FAAAAAAAAAAA='
        break;
    case '8':
        msg.payload.data = 'JgBQAAABKJURFBEUERQSFBEUERQRFBITEjgROBI4ETgSOBE4EjgROBE4EhQROBITETkRFBITEhMSFBE4EhMSOBAVEjcSOBE4EgAFHgABKEsRAA0FAAAAAAAAAAA='
        break;
    case '9':
        msg.payload.data = 'JgBQAAABKJYRFBEUEhMSExIUEBURFBITEjgROBI4ETgSOBE4EjgROBITEjgROBITEjgRFBITEhMSOBEUEhMSOBEUEjgROBE4EgAFHgABJ0wSAA0FAAAAAAAAAAA='
        break;
    case '10':
        msg.payload.data = 'JgBYAAABKJURFBITEhMRFREUERQRFBITEjgROBI4ETgSOBE4EjcRORITEhMSFBE4EjcSFBEUERQSOBE4EjgRFBEUETgSOBE4EgAFHgABKEsSAAxRAAEnTBAADQU='
        break;
    case '11':
        msg.payload.data = 'JgBQAAABKZURFBITEhMSFBEUEhMSExEVETgSOBE4EjgROBE5ETgSOBE4EhMSFBE4EjgRFBEUEhMSFBE4EjgRFBEUEjgROBI4EQAFHwABKEsRAA0FAAAAAAAAAAA='
        break;
    case '12':
        msg.payload.data = 'JgBQAAABKZURFBITEhQRFBITEhMRFREUETgSOBE4EjgROBI4ETgSOBEUETgSExI4EjcSFBEUEhMSOBEUETgSExIUETgSOBE4EgAFHgABKUoSAA0FAAAAAAAAAAA='
        break;
}

return msg;
